const util = require('util');
const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const fs = require('fs');

let mqtt = require('mqtt');
let app = express();
let Port = 8181;
let connectedDevices = [];
let alarms = [];
// Read the certificate needed for MQTT.
let CERT = fs.readFileSync('./cacert.pem');

// Set mqtt login options.
var options = {
	host: process.env.MQTT_HOST,
	port: process.env.MQTT_PORT,
	connectTimeout: 1000,
	ca: CERT,
	protocol: 'mqtts',
	rejectUnauthorized: false,
	// protocolId: 'MQIsdp', //It works without this.
	// protocolVersion: 3 // It works without this.
};

dotenv.config();
app.use(cors());

options.username = process.env.MQTT_USER;
options.password = process.env.MQTT_PASS;

// Create the mqtt client
let client = mqtt.connect(process.env.MQTT_HOST, options);

/*
// Here we tried to turn off alarms by publishing to the mqtt server. 
// This never worked, but we think some code might be useful, and therefore did not delete it.

options.username = process.env.MQTT_PUB_USER;
options.password = process.env.MQTT_PUB_PASS;

let publisher = mqtt.connect(process.env.MQTT_HOST, options);

function disableAlarm(clockId) {
	let s =
		'mtble/' +
		process.env.MQTT_GATEWAY_MAC +
		'/command/device/' +
		clockId +
		'/clearalarm';
	let s2 = '{"id": "' + Math.floor(Math.random() * 10000) + '"}';
	publisher.publish(s, s2);
}

publisher.on('connect', () => {
	console.log('Publisher connected!');
	let s1 = "{ id: 'cefb923f175d' }";
	let s2 = "{ id: 'd31e9c4ba337' }";
	publisher.subscribe(
		'mtble/' +
			process.env.MQTT_GATEWAY_MAC +
			'/response/device/' +
			process.env.CLOCK2 +
			'/clearalarm'
	);
});

publisher.on('message', (topic, message) => {
	console.log(message.toString());
});
*/
// When the client connects
client.on('connect', () => {
	// Subscribe to alarm and position topics.
	client.subscribe('alarm/#');
	client.subscribe('position/#');
	console.log("I'm connected to the MQTT server");
});

// If the client disconnects and reconnects - print that it its reconnecting.
client.on('reconnect', () => {
	console.log('I disconnected from the MQTT server - RECONNECTING');
});

// When the client gets a message ...
client.on('message', (topic, message) => {
	let m = JSON.parse(message.toString());
	// Log the message
	console.log(m);

	// If its a message from position
	if (topic.includes('position')) {
		alarms.forEach((a) => {
			// Update each alarm with the parent_id to always have the watch's latest known position
			if (a.id === m.device_id) {
				a['last_known_parent'] = m.parent_id;
			}
		});
	}
	// If its a message from alarm
	if (topic.includes('alarm')) {
		// check if this device has already sent a message before ...
		if (
			!connectedDevices.includes(m.device_id) &&
			m.device_id != undefined
		) {
			// If not, push the id to connected devices
			connectedDevices.push(m.device_id);
			// And create a new alarm object for this device.
			alarms.push({
				id: m.device_id,
				status: m.alarm_status,
				time: m.time,
			});
		} else {
			// If the alarm has sent a message before...
			for (let i = 0; i < alarms.length; i++) {
				if (alarms[i].id === m.device_id) {
					// ...Check if the alarm status is updated
					if (alarms[i].status != m.alarm_status) {
						// And update it's state and time for showing on the dashboard.
						alarms[i].status = !alarms[i].status;
						alarms[i].time = m.time;
					}
				}
			}
		}
	}
});

// Send all alarms to the endpoint
app.get('/', (req, res) => {
	res.send(alarms);
});

app.listen(Port, () => {
	console.log('Express MQTT at [ localhost:' + Port + ' ]');
});
