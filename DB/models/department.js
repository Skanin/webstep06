const mongoose = require('mongoose');

// Schema for departments (this one is not used as of 24.04.2020)
const departmentSchema = mongoose.Schema({
	department: {
		type: String,
		require: false,
		unique: true,
	},
	name: {
		type: String,
		require: true,
		unique: true,
	},
	room: [Array],
});

module.exports = mongoose.model('department', departmentSchema);
