const mongoose = require('mongoose');

// Schema for patients.
const patientSchema = mongoose.Schema({
	// Patients have a firstname...
	firstname: {
		type: String,
		require: true,
	},
	// ...eventual middlename...
	middlename: {
		type: String,
		require: false,
	},
	// ... lastname...
	lastname: {
		type: String,
		require: true,
	},
	// ...Social Security Number...
	pnr: {
		type: Number,
		unique: true,
		require: true,
	},
	// ...fullname (logic to automatically set fullname is in DB/routes/patient.js)...
	fullname: {
		type: String,
		require: true,
	},
	// ...device id, this is their watch ...
	device_id: {
		type: String,
		require: false,
		unique: false,
		default: '',
	},
	// ...an image of the patient. Also a default image is none is given ...
	IMG: {
		type: String,
		require: true,
		default: 'https://www.computerhope.com/jargon/g/guest-user.jpg',
	},
	// ... Their checked in status ...
	checked_in: {
		type: Boolean,
		require: true,
		default: false,
	},
	// ... And a medical history.
	medicalHistory: {
		type: String,
		require: false,
	},
});

module.exports = mongoose.model('patient', patientSchema);
