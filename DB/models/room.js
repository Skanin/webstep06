const mongoose = require('mongoose');

// Schema for rooms
const roomSchema = mongoose.Schema({
	// Parent_id is the mac adress for the bluetooh moudule in the room
	parent_id: {
		type: String,
		require: true,
		unique: true,
	},
	// Name for the room (I.e A1, 203 ...)
	name: {
		type: String,
		require: true,
		unique: false,
	},
	// Which patient that lives there (their ssn).
	owner: {
		type: Number,
		require: false,
	},
	// If the room is available.
	available: {
		type: Boolean,
		default: true,
	},
});

module.exports = mongoose.model('room', roomSchema);
