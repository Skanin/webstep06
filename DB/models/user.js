const mongoose = require('mongoose');

// Schema for caregivers.
const userSchema = mongoose.Schema({
	device_id: {
		type: String,
		default: null,
	},
	// Caregivers have a firstname ...
	firstname: {
		type: String,
		require: true,
	},
	// ... eventually a middlename...
	middlename: {
		type: String,
		require: false,
	},
	// ... a lastname ...
	lastname: {
		type: String,
		require: true,
	},
	// ...an employee id ...
	employeeID: {
		type: Number,
		unique: true,
		require: true,
	},
	// ...a fullname, logic to automatically set the fullname can be found in DB/routes/user.js ...
	fullname: {
		type: String,
		require: true,
	},
	// ... a device_id, which is the caregiver's watch...
	device_id: {
		type: String,
		require: false,
		unique: false,
		default: null,
	},
	// ... an image of the cargegiver...
	IMG: {
		type: String,
		require: true,
		default: '../img/caduceus.png',
	},
	// ...availability status ...
	available: {
		type: Boolean,
		require: true,
		default: false,
	},
	// ... Username used for login...
	username: {
		type: String,
		require: true,
	},
	// ...Their password.
	password: {
		type: String,
		require: true,
	},
});

module.exports = mongoose.model('users', userSchema);
