const mongoose = require('mongoose');

// Schema for wristbands
const wristbandSchema = mongoose.Schema({
	// Each wristband has a name
	name: {
		type: String,
		require: true,
		unique: false,
	},
	// If it is available
	available: {
		type: Boolean,
		default: true,
	},
	// It's id
	deviceID: {
		type: String,
		require: true,
	},
	// And which patient that has this wristband.
	owner: {
		type: Number,
		default: null,
	},
});

module.exports = mongoose.model('wristband', wristbandSchema);
