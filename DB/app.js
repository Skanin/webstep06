let URI = 'mongodb+srv://gruppe06:mongo123mongo@gruppe06-f89bj.mongodb.net/NeuCaduceusDB';
const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const cors = require('cors');
const app = express();
let Port = process.env.PORT || 3002;


// Solve the Cross-origin resource sharing and JSON parsing problem  
app.use(cors());
app.use(bodyparser.json());

// Imported Routes
const userRoute = require('./routes/user');
const patientRoute = require('./routes/patient');
const deviceRoute = require('./routes/device');
const roomRoute = require('./routes/room');
const deparmentRoute = require('./routes/department');
const wristbandRoute = require('./routes/wristband');

// Routes
app.use('/user', userRoute);
app.use('/patient', patientRoute);
app.use('/device', deviceRoute);
app.use('/room', roomRoute);
app.use('/department', deparmentRoute);
app.use('/wristband', wristbandRoute);


//Connects to th Mongo Atlas DB
mongoose .connect(URI, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false}, () => {
	console.log('Connected to DB');
})
.catch((res) => console.log(res));

// Manteins a socket runnig for the application at port PORT
app.listen(Port, () => {
	console.log('Express at [ localhost:' + Port + ' ]');
});
