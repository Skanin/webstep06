//Hente ut fra parent-id eller manipulere
const express = require('express');
const router = express.Router();
const Room = require('../models/room');
const Patient = require('../models/patient');
const ObjectId = require('mongoose').Types.ObjectId;

// Get all rooms
router.get('/all', async (req, res) => {
	try {
		const rooms = await Room.find();
		res.send(rooms);
	} catch (error) {
		res.send(error);
	}
});

// Get all room that is avaliable
router.get('/available', async (req, res) => {
	try {
		const rooms = await Room.find({ available: true });
		res.send(rooms);
	} catch (error) {
		res.send(error);
	}
});

//Find owner of room based on parent_id
router.get('/owner/:parent_id', async (req, res) => {
	try {
		Room.findOne(req.params)
			.then((room) => Patient.findOne({ pnr: room.owner }))
			.then((patientOwner) => res.send(patientOwner));
	} catch (error) {
		res.send(error);
	}
});

//Find room by using parent_id
router.get('/name/:parent_id', async (req, res) => {
	Room.findOne({ parent_id: req.params.parent_id })
		.then((room) => res.send(room))
		.catch((err) => res.send(err));
});

// compare if correct
router.get('/login/user=:user', async (req, res) => {
	try {
		Room.findOne({ parent_id: req.params.parent_id });
		res.send(rooms.name);
	} catch (error) {
		res.send(error);
	}
});

// Get room based on their DB id.
router.get('/id/:_id', async (req, res) => {
	try {
		const room = await Room.findOne({ _id: new ObjectId(req.params._id) });
		res.send(room);
	} catch (err) {
		res.send(err);
	}
});

// Update a room's availability and owner status.
router.put(
	'/update/available=:available&_id=:_id&owner=:owner',
	async (req, res) => {
		let status = req.params.available === 'true';
		let owner =
			req.params.owner === 'null' ? null : parseInt(req.params.owner);
		Room.updateOne(
			{ _id: new ObjectId(req.params._id) },
			{ $set: { available: status, owner: owner } }
		)
			.then((result) => res.sendStatus(200).send(result))
			.catch((error) => res.sendStatus(400).send({ message: error }));
	}
);

// router.get('/ID=:parent_id', async (req, res) => {
// 	try {
// 		// console.log(req.params);
// 		const room = await Room.find({ parent_id: req.params.parent_id });
// 		res.send(room);
// 	} catch (error) {
// 		res.send({ message: 'No match' });
// 	}
// });

// //Legge til nytt rom
// router.post('/new', (req, res) => {
// 	console.log(req);
// 	const room = new Room({
// 		parent_id: req.query.parent_id,
// 		room: req.query.room
// 	});
// 	room.save()
// 		.then(data => {
// 			res.json(data);
// 		})
// 		.catch(err => {
// 			res.send(err);
// 		});
// });

module.exports = router;
