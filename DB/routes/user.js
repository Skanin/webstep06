const express = require('express');
const router = express.Router();
const User = require('../models/user');

// Returns all User in the DB
router.get("/all", async (req, res) => {
    try {
        const users = await User.find();
        res.send(users);
    } catch (error) {
        res.send(error);
    }
});

// Parameter: Name of the User 
// Returns:  A list of person-objects fmathcing the most to the name
router.get("/name=:name", async (req, res) => {
    try {
        User.find(
            {
                $text: {
                    $search: req.params.name.split("+").join(" "),
                    $caseSensitive: false,
                    $diacriticSensitive: false,
                },
            },
            { score: { $meta: "textScore" } }
        )
            .sort({ score: { $meta: "textScore" } })
            .then((users) => res.status(200).send(users));
    } catch (error) {
        res.status(400).json({ message: error });
    }
});


// Parameter:The employee identifiactions number
// Returns:  The employee/User 


// Get user(s) by using employeeID
router.get('/employeeID=:ID', async (req, res) => {
	try {
		User.find({ employeeID: req.params.ID }).then((user) =>
			res.status(200).send(user)
		);
	} catch (error) {
		res.status(400).json({ message: error });
	}
});

// Parameter:
// Returns: 

// Update available state of an user by using employeeID
router.put('/update/employeeID=:ID&available=:available', async (req, res) => {
	let status = req.params.available === 'true';
	User.updateOne(
		{ employeeID: req.params.ID },
		{ $set: { available: status } }
	)
		.then((result) => res.status(200).send(result))
		.catch((error) => res.status(400).send({ message: error }));
});

// Parameter:sss
// Returns: 
// Get status of current avala state by using PNR
router.get("/available/employeeID=:employeeID", async (req, res) => {
    User.findOne({ employeeID: req.params.employeeID })
        .then((user) => res.status(200).send(user.available))
        .catch((err) => res.send(400).send({ message: err }));
});



// Parameter:
// Returns: 

// compare if correct
router.get('/login/user=:user', async (req, res) => {
	try {
		const user = await User.findOne({ username: req.params.user });
		res.status(200).send(user);
	} catch (error) {
		res.send(error);
	}
});

// Parameter:
// Returns: 

// Post a new user in the system
router.post('/new', (req, res) => {
	let names;
	req.body.middlename !== ''
		? (names =
				req.body.firstname +
				' ' +
				req.body.middlename +
				' ' +
				req.body.lastname)
		: (names = req.body.firstname + ' ' + req.body.lastname);
	const user = new User({
		firstname: req.body.firstname,
		middlename: req.body.middlename,
		lastname: req.body.lastname,
		fullname: names,
		employeeID: req.body.employeeID,
		device_id: req.body.device_id,
		IMG: req.body.IMG,
		available: req.body.available,
		username: req.body.username,
		password: req.body.password,
	});
	user.save()
		.then((data) => {
			res.status(200).json(data);
		})
		.catch((err) => {
			res.status(400).send(err);
		});
});

// Update user information
router.post('/updateuser/pnr=:pnr', async (req, res) => {
	let names;
	req.body.middlename !== ''
		? (names =
				req.body.firstname +
				' ' +
				req.body.middlename +
				' ' +
				req.body.lastname)
		: (names = req.body.firstname + ' ' + req.body.lastname);
	User.updateOne(
		{ pnr: req.body.pnr },
		{
			$set: {
				firstname: req.body.firstname,
				middlename: req.body.middlename,
				lastname: req.body.lastname,
				fullname: names,
				pnr: req.body.pnr,
				medicalHistory: req.body.medicalHistory,
			},
		}
	)
		.then((result) => res.status(200).send(result))
		.catch((error) => res.status(400).send({ message: error }));
});

// Update check_in state of an user by using SSN
router.put('/update/pnr=:pnr&checked_in=:status', async (req, res) => {
	let status = req.params.status == 'true';
	console.log(status + typeof status);
	User.updateOne({ pnr: req.params.pnr }, { $set: { checked_in: status } })
		.then((result) => res.status(200).send(result))
		.catch((error) => res.status(400).send({ message: error }));
});

// Get status of current check_in state by using SSN
router.get('/check_in/pnr=:pnr', async (req, res) => {
	User.findOne({ pnr: req.params.pnr })
		.then((user) => res.status(200).send(user.checked_in))
		.catch((err) => res.send(400).send({ message: err }));
});

module.exports = router;
