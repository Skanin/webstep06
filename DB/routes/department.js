//Hente ut fra parent-id eller manipulere
const express = require('express');
const router = express.Router();
const Department = require('../models/department');

// Get all the different departments
router.get('/all', async (req, res) => {
	try {
		console.log(req.params);
		// console.log(req.params);
		const department = await Department.find();
		res.send(department);
	} catch (error) {
		res.send(error);
	}
});

// Search for room by id
router.get('/room=:rooms', async (req, res) => {
	try {
		console.log(req.params);
		const department = await Department.find(req.params);
		res.send(department);
	} catch (error) {
		res.send(error);
	}
});

module.exports = router;
