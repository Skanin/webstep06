//Hente ut fra klokken-id eller manipulere
const express = require('express');
const router = express.Router();
const User = require('../models/user');

// Get user based on device id.
router.get('/ID=:device_id', async (req, res) => {
	try {
		console.log(req.params);
		const user = await User.find({ device_id: req.params.device_id });
		res.send(user);
	} catch (error) {
		res.send({ message: 'No match' });
	}
});

module.exports = router;
