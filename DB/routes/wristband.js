const express = require('express');
const router = express.Router();
const Wristband = require('../models/wristband');
const ObjectId = require('mongoose').Types.ObjectId;
const Patient = require('../models/patient');

// Returns all wristbands
router.get('/all', async (req, res) => {
	try {
		const wristbands = await Wristband.find();
		console.log(wristbands);
		res.send(wristbands);
	} catch (error) {
		res.send(error);
	}
});

// Returns wristbands available
router.get('/available', async (req, res) => {
	try {
		const wristbands = await Wristband.find({ available: true });
		res.send(wristbands);
	} catch (error) {
		res.send(error);
	}
});

// Return wristband with spesific ID
router.get('/id/:_id', async (req, res) => {
	try {
		const wristband = await Wristband.findOne({
			_id: new ObjectId(req.params._id),
		});
		res.send(wristband);
	} catch (err) {
		res.send(err);
	}
});

// Parameters: Device ID for the wristbands
// Returns: Patient Object who is the owner of the wristband
router.get('/owner/:deviceID', async (req, res) => {
	try {
		console.log(req.params);
		Wristband.findOne({ deviceID: req.params.deviceID })
			.then((wristband) => Patient.findOne({ pnr: wristband.owner }))
			.then((patient) => res.status(200).send(patient));
	} catch (err) {
		res.send(err);
	}
});

// Parameters: PNR of a Patient
// Returns: If they own a wristband, the wristbandobject is returns. Else, an error. 
router.get('/owner=:owner', async (req, res) => {
	try {
		const wristband = await Wristband.find({ owner: req.params.owner });
		res.send(wristband);
	} catch (error) {
		res.send(error);
	}
});

// Parameters: Wanted availability status and the persons PNR 
// Returns: The updatet version of the person, or else, and error message
router.put('/update/available=:available&_id=:_id&owner=:owner',
	async (req, res) => {
		let status = req.params.available === 'true';
		let owner =
			req.params.owner === 'null' ? null : parseInt(req.params.owner);
		Wristband.updateOne(
			{ _id: new ObjectId(req.params._id) },
			{ $set: { available: status, owner: owner } }
		)
			.then((result) => res.sendStatus(200).send(result))
			.catch((error) => res.sendStatus(400).send({ message: error }));
	}
);


module.exports = router;
