const express = require('express');
const router = express.Router();
const User = require('../models/patient');
const Room = require('../models/room');
const Patient = require('../models/patient');

// Get patient(s) by name
router.get('/name=:name', async (req, res) => {
	try {
		let results = User.find(
			{
				$text: {
					$search: req.params.name.split('+').join(' '),
					$caseSensitive: false,
					$diacriticSensitive: false,
				},
			},
			{ score: { $meta: 'textScore' } }
		)
			.sort({ score: { $meta: 'textScore' } })
			.then((users) => res.status(200).send(users));
	} catch (error) {
		res.status(400).json({ message: error });
	}
});

// Get patient(s) by using SSN
router.get('/pnr=:pnr', async (req, res) => {
	try {
		User.find({ pnr: req.params.pnr }).then((user) =>
			res.status(200).send(user)
		);
	} catch (error) {
		res.status(400).json({ message: error });
	}
});

// Post a new patient
router.post('/new', (req, res) => {
	let names;
	req.body.middlename !== ''
		? (names =
				req.body.firstname +
				' ' +
				req.body.middlename +
				' ' +
				req.body.lastname)
		: (names = req.body.firstname + ' ' + req.body.lastname);
	const user = new User({
		firstname: req.body.firstname,
		middlename: req.body.middlename,
		lastname: req.body.lastname,
		fullname: names,
		pnr: req.body.pnr,
		device_id: req.body.device_id,
		IMG: req.body.IMG,
		checked_in: req.body.checked_in,
		medicalHistory: req.body.medicalHistory,
	});
	user.save()
		.then((data) => {
			res.status(200).json(data);
		})
		.catch((err) => {
			res.status(400).send(err);
		});
});

// Update check_in state of a patient by using SSN
router.put('/update/pnr=:pnr&checked_in=:status', async (req, res) => {
	let status = req.params.status == 'true';
	User.updateOne({ pnr: req.params.pnr }, { $set: { checked_in: status } })
		.then((result) => res.status(200).send(result))
		.catch((error) => res.status(400).send({ message: error }));
});

// Get status of current cehck_in state by using SSN
router.get('/check_in/pnr=:pnr', async (req, res) => {
	User.findOne({ pnr: req.params.pnr })
		.then((user) => res.status(200).send(user.checked_in))
		.catch((err) => res.send(400).send({ message: err }));
});

// Checkout by updating room and patient
router.put('/checkout/pnr=:pnr', async (req, res) => {
	Patient.findOneAndUpdate(
		{ pnr: req.params.pnr },
		{ $set: { checked_in: false } },
		{ returnNewDocument: true }
	)
		.then((updatedPatient) => {
			Room.updateOne(
				{ owner: updatedPatient.pnr },
				{ $set: { owner: null, available: true } }
			).then((message) => res.status(200).send(message));
		})
		.catch((err) => res.status(400).send(err));
});

// Checkin by updating room and patient
router.put('/checkin/pnr=:pnr&room=:room', async (req, res) => {
	Patient.findOneAndUpdate(
		{ pnr: req.params.pnr },
		{ $set: { checked_in: true } },
		{ returnNewDocument: true }
	)
		.then((updatedPatient) => {
			Room.updateOne(
				{ parent_id: req.params.room },
				{ $set: { owner: updatedPatient.pnr, available: false } }
			).then((mess) => res.status(200).send(mess));
		})
		.catch((err) => res.status(400).send(err));
});

// Get username of caregiver
router.get('/login/user=:user', async (req, res) => {
	try {
		const user = await User.findOne({ username: req.params.user });
		res.status(200).send(user);
	} catch (error) {
		res.send(error);
	}
});

// Update check_in state of an user by using SSN
router.put('/update/pnr=:pnr&checked_in=:status', async (req, res) => {
	let status = req.params.status == 'true';
	User.updateOne({ pnr: req.params.pnr }, { $set: { checked_in: status } })
		.then((result) => res.status(200).send(result))
		.catch((error) => res.status(400).send({ message: error }));
});

// Get status of current cehck_in state by using SSN
router.get('/check_in/pnr=:pnr', async (req, res) => {
	User.findOne({ pnr: req.params.pnr })
		.then((user) => res.sendStatus(200).send(user.checked_in))
		.catch((err) => res.sendStatus(400).send({ message: err }));
});

// Get patient's room based on SSN.
router.get('/room/pnr=:pnr', async (req, res) => {
	Room.findOne({ owner: req.params.pnr })
		.then((room) => {
			room === null ? res.send({}) : res.send(room);
		})
		.catch((err) => {
			res.send(err);
		});
});

// Update patient information
router.post('/updateInfo/pnr=:pnr', async (req, res) => {
	let names;
	req.body.middlename !== ''
		? (names =
				req.body.firstname +
				' ' +
				req.body.middlename +
				' ' +
				req.body.lastname)
		: (names = req.body.firstname + ' ' + req.body.lastname);
	Patient.updateOne(
		{ pnr: req.body.pnr },
		{
			$set: {
				firstname: req.body.firstname,
				middlename: req.body.middlename,
				lastname: req.body.lastname,
				fullname: names,
				pnr: req.body.pnr,
				medicalHistory: req.body.medicalHistory,
			},
		}
	)
		.then((result) => res.status(200).send(result))
		.catch((error) => res.status(400).send({ message: error }));
});

module.exports = router;
