import React, { Component } from 'react';
import './SearchPersons.css';
import Person from '../Person/Person';
const { getPatient, getRoom, getPatientFromName } = require('../utils/patient');

export default class SearchPersons extends Component {
	constructor() {
		super();
		this.state = {
			result: [],
		};
		this.handleClick = this.handleClick.bind(this);
		this.personClick = this.personClick.bind(this);
	}

	// Handle button click
	async handleClick() {
		// Set the states to default values.
		this.setState({
			chosen: false,
			result: [],
		});

		// Get the inputed value
		let value = document.getElementById('nameSearch').value;

		// If it is a ssn seatch ...
		if (!isNaN(value)) {
			// ...get patient and room based on ssn.
			let patient = await getPatient(value);
			let room = await getRoom(value);

			// Set the state with the patient returned from fetch above.
			this.setState({
				result: [
					{
						firstname: patient.firstname,
						lastname: patient.lastname,
						pnr: patient.pnr,
						device_id: patient.device_id,
						medicalHistory: patient.medicalHistory,
						middlename: patient.middlename,
						checked_in: patient.checked_in,
						room:
							Object.keys(room).length === 0 &&
							room.constructor === Object
								? 'Ingen rom'
								: room.name,
						IMG: patient.IMG,
					},
				].map((p) => (
					// Map the patient to a person component, with the patient's info as props + onclick function
					<Person key={p.pnr} info={p} onClick={this.personClick} />
				)),
			});
			// If the search is not a ssn...
		} else {
			// ...get the patient based on name
			let patients = await getPatientFromName(value);

			// Loop trough all returned patients...
			for (let i = 0; i < patients.length; i++) {
				let p = patients[i];
				let room = await getRoom(p.pnr);
				// Update their room...
				p.room =
					Object.keys(room).length === 0 &&
					room.constructor === Object
						? 'Ingen rom'
						: room.name;
			}
			this.setState({
				// ...and set state with all returned patients, mapped to person components.
				result: patients.map((p) => (
					<Person key={p.pnr} info={p} onClick={this.personClick} />
				)),
			});
		}
	}

	// This is fired when a person component is clicked. The param value is a PersonExtra component.
	personClick(value) {
		// Set state to know that a patient is selected.
		this.setState({
			chosen: true,
			person: value,
		});
	}

	render() {
		return (
			<div className='searchPersons'>
				<div id='SearchSection'>
					<h1>Søk etter brukere</h1>
					<input
						id='nameSearch'
						type='text'
						value={this.state.value}
					/>
					<input
						id='searchBTN'
						onClick={this.handleClick}
						type='button'
						value='Search'
					/>
					<div className='retrievedPersons'>{this.state.result}</div>
				</div>
				<div className='moreInfo'>
					<h1>Mer informasjon om valgt bruker:</h1>
					{!this.state.chosen ? (
						<div> Søk og velg en bruker.</div>
					) : (
						this.state.person
					)}
				</div>
			</div>
		);
	}
}
