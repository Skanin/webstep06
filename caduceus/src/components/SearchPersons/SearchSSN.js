export function searchBySSN(pnr) {
	return fetch('http://localhost:3002/patient/pnr=' + pnr)
		.then((result) => {
			return result.json();
		})
		.then((data) => {
			return data[0];
		})
		.catch();
}
