import React, { Component } from 'react';
import './Alarm.css';
import ExtraInfo from '../ExtraInfo/ExtraInfo';
const fetch = require('node-fetch');

/**
 * This component will render an alarm. It get's data from the body component and renders this data to the application
 */
export class Alarm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isExtraAvailable: false,
			isNameVisible: true,
			id: this.props.id,
			name: 'Loading...',
			time: '00:00',
			alarm: 'Klokke',
			secs: 0,
			initialData: null,
			room: 'Default',
		};
	}

	componentDidMount() {
		// Fetch the owner of the wristband that fired an alarm and set this alarm's state to their info.
		fetch('http://localhost:3002/wristband/owner/' + this.props.id)
			.then((results) => results.json())
			.then((data) => {
				this.setState({
					initialData: data,
					pictures: data.IMG,
					name: data.firstname,
				});
			})
			.catch((err) => {
				this.setState({
					initialData: null,
					name: 'Unknown',
					pictures: 'caduceus/src/img/defaultProfilePic.png',
				});
			});

		// Update the patient's room (it is stored in another table in the DB).

		// Set the 'time since alarm went off'
		this.setTime(new Date(this.props.time));
		this.tick();
		// Set the component to update the timer every second
		this.intervalID = setInterval(() => this.tick(), 1000);
		this.setTime(new Date(this.props.time));
	}

	tick() {
		// Update the loaction of the patient every second
		this.updateRoom();
		// Update the 'time since alerm went off' every second
		this.setState({
			time: this.hhmmss(this.state.secs),
			secs: this.state.secs + 1,
		});
	}

	updateRoom = () => {
		// Fetch the room the patient is closest to and set the state of this alarm
		fetch('http://localhost:3002/room/name/' + this.props.parent)
			.then((responde) => responde.json())
			.then((room) => this.setState({ room: room.name }))
			.catch((err) => console.log('WROND ' + this.props.parent));
	};

	// Pad the timer with zeroes
	pad(num) {
		return ('0' + num).slice(-2);
	}

	// Convert secondt to HH:MM:SS (for the timer).
	hhmmss(secs) {
		var minutes = Math.floor(secs / 60);
		secs = secs % 60;
		minutes = minutes % 60;
		return `${this.pad(minutes)}:${this.pad(secs)}`;
	}

	// Get the time from the alarm and calculate the difference between when the alarm went off and when the system picked it up
	setTime(time) {
		let diff = Math.ceil((new Date().getTime() - time.getTime()) / 1000);
		this.setState({ secs: diff });
	}

	render() {
		return (
			<div className='alarmcontainer'>
				<div
					className='alarm'
					onClick={() => {
						this.setState({
							isExtraAvailable: !this.state.isExtraAvailable,
							isNameVisible: !this.state.isNameVisible,
						});
					}}>
					<img
						className='image'
						src={this.state.pictures}
						alt='Loading...'
					/>
					<div className='alarmRoom'>{this.state.room}</div>
					<div className='alarmName'>{this.state.name}</div>
					<div className='alarmTime'>{this.state.time}</div>
					<div className='alarmSent'>{this.state.alarm}</div>
				</div>
				{this.state.isExtraAvailable ? (
					<ExtraInfo data={this.state.initialData} />
				) : null}
			</div>
		);
	}
}

export default Alarm;
