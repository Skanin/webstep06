import React, { Component } from 'react';
import axios from 'axios';

/**
 * This component makes it possible to change a patient's info in the database.
 */
export default class PersonEdit extends Component {
	constructor(props) {
		super(props);
		this.state = {
			message: '',
		};
	}

	// Function to handle a change event of the inputs.
	handleChange(event) {
		event.preventDefault();

		let data = this.props.data;
		// Switch based on the ID the HTML element that is changed. Used to set the component state with the inputed value.
		switch (event.target.id) {
			case 'firstname':
				data.firstname = event.target.value;
				this.setState({ data });
				break;
			case 'middlename':
				data.middlename = event.target.value;
				this.setState({ data });
				break;
			case 'lastname':
				data.lastname = event.target.value;
				this.setState({ data });
				break;
			case 'pnr':
				data.pnr = event.target.value;
				this.setState({ data });
				break;
			case 'medicalHistory':
				data.medicalHistory = event.target.value;
				this.setState({ data });
				break;
			default:
				break;
		}
	}

	// Function to handle submit of the form
	handleSubmit(event) {
		// Prevent the form from reloading the page.
		event.preventDefault();

		// Fetch the patient to be updated from the database
		fetch('http://localhost:3002/patient/pnr=' + this.props.data.pnr)
			.then((res) => {
				return res.json();
			})
			.then((d) => {
				try {
					axios
						// Post the new changes to the database
						.post(
							'http://localhost:3002/patient/updateInfo/pnr=' +
								this.props.data.pnr,
							this.props.data
						)
						// Then let the user know the patient has been updated
						.then((res) => {
							this.setState({
								message: `${this.props.data.firstname} ble oppdatert!`,
							});
						});
				} catch (err) {
					console.log(err);
				}
			});
	}

	render() {
		return (
			<div>
				Rediger informasjon
				<div id='formDiv'>
					<form onSubmit={(e) => this.handleSubmit(e)}>
						<label>
							Fornavn:
							<input
								id='firstname'
								type='text'
								placeholder='Fornavn'
								required
								defaultValue={this.props.data.firstname}
								onChange={(e) => this.handleChange(e)}></input>
						</label>
						<label>
							Mellomnavn:
							<input
								id='middlename'
								type='text'
								placeholder='Mellomnavn'
								defaultValue={this.props.data.middlename}
								onChange={(e) => this.handleChange(e)}></input>
						</label>
						<label>
							Etternavn:
							<input
								id='lastname'
								type='text'
								required
								placeholder='Etternavn'
								defaultValue={this.props.data.lastname}
								onChange={(e) => this.handleChange(e)}></input>
						</label>
						<label>
							Diagnoser:
							<textarea
								id='medicalHistory'
								type='textarea'
								placeholder='Diagnoser'
								defaultValue={this.props.data.medicalHistory}
								onChange={(e) =>
									this.handleChange(e)
								}></textarea>
						</label>

						<button type='submit'>Oppdater pasient</button>
					</form>
					{this.state.message !== '' ? (
						<p>{this.state.message}</p>
					) : null}
				</div>
				<button
					onClick={() => {
						this.props.editInfo();
					}}>
					Tilbake
				</button>
			</div>
		);
	}
}
