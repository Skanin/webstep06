import React, { Component } from 'react';
import './Person.css';
import PersonExtra from '../Person/PersonExtra';

/**
 * This component renderers a patient's info (after search). It gets it's info from the SearchPersons-component.
 */
export default class Person extends Component {
	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	// Handle click on the person object.
	handleClick() {
		// Create a PersonExtra component with props as the patient's info
		const person = (
			<PersonExtra
				data={this.props.info}
				pnr={this.props.info.pnr}
				device_id={this.props.info.device_id}
				checked_in={this.props.info.checked_in}
				room={this.props.info.room}
				IMG={this.props.info.IMG}
				edit={false}
			/>
		);
		// Fire the personclick function in SearchPersons
		this.props.onClick(person);

		// Update styling to show what patient was clicked,
		let persons = document.getElementsByClassName('person');

		for (let person of persons) {
			person.className = 'person';
		}
		let colorChange = document.getElementById(this.props.info.pnr);

		colorChange.className = 'person clicked';

		let moreInfoChange = document.getElementsByClassName('moreInfo');
		moreInfoChange[0].className = 'moreInfo moreInfoClicked';
	}

	render() {
		return (
			<div onClick={this.handleClick}>
				<div className='person' id={this.props.info.pnr}>
					<div className='firstname'>{this.props.info.firstname}</div>
					<div className='surname'>{this.props.info.lastname}</div>
					<div className='pnr'>{this.props.info.pnr}</div>
				</div>
			</div>
		);
	}
}
