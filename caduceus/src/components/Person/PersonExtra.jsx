import React, { Component } from 'react';
import PersonEdit from './PersonEdit';
const { checkInOutPatient, getPatient, getRoom } = require('../utils/patient');
const { getRandomRoom } = require('../utils/room');

/**
 * This component will render extra info about a person, if that person is clicked (in search)
 */
export default class PersonExtra extends Component {
	constructor(props) {
		super(props);
		this.state = {
			edit: false,
			updated: 0,
		};
		this.checkIn = this.checkIn.bind(this);
		this.chekOut = this.checkOut.bind(this);
		this.editInfo = this.editInfo.bind(this);
	}

	componentDidMount() {
		// Set state from props
		this.setState({
			checked_in: this.props.checked_in,
			pnr: this.props.pnr,
			room: this.props.room,
		});
	}

	async componentDidUpdate(prevProps, prevState) {
		// Ensure that the person has updated room and checked in status.
		let user = await getPatient(this.props.pnr);
		let room = await getRoom(this.props.pnr);

		if (user.checked_in !== this.state.checked_in) {
			this.setState({
				checked_in: user.checked_in,
				room: Object.keys(room).length === 0 ? 'Ingen rom' : room.name,
			});
		}
		// Check if the change of the component was edit-change or checkin/out change
		if (prevState.edit && !this.props.edit) {
			this.setState({
				edit: false,
			});
		}
	}

	// Check in the selected patient
	async checkIn() {
		// Get random room
		let room = await getRandomRoom();
		// Check the patient out
		await checkInOutPatient(this.state.pnr, true, room);
		// Set state for an update of the page.
		this.setState({
			updated: this.state.updated + 1,
		});
	}

	// Check out the selected patient
	async checkOut() {
		// Check them out
		await checkInOutPatient(this.state.pnr, false);
		// Set state for an update of the page.
		this.setState({
			updated: this.state.updated + 1,
		});
	}

	editInfo() {
		this.setState({ edit: !this.state.edit });
	}

	render() {
		return (
			<div>
				{!this.state.edit ? (
					<div>
						<h1>
							{this.props.data.firstname}{' '}
							{this.props.data.middlename}{' '}
							{this.props.data.lastname}
						</h1>
						<img
							src={this.props.IMG}
							alt={this.props.data.firstname}
							height='75'
							width='75'></img>
						<div>KlokkeID: {this.props.device_id}</div>
						<div>Rom: {this.state.room}</div>
						<div>Fødselsnummer: {this.props.pnr}</div>
						<div>
							Sjekket inn?{' '}
							<b>{this.state.checked_in ? 'Ja' : 'Nei'}</b>
							{this.state.checked_in ? (
								<button onClick={() => this.checkOut()}>
									Sjekk ut
								</button>
							) : (
								<button onClick={() => this.checkIn()}>
									Sjekk inn
								</button>
							)}
						</div>
						<div>
							<h2>Medisinsk historie</h2>
							{this.props.data.medicalHistory}
						</div>
						<button onClick={this.editInfo}>Edit info</button>
					</div>
				) : (
					<PersonEdit
						data={this.props.data}
						editInfo={this.editInfo}
					/>
				)}
			</div>
		);
	}
}
