import React, { Component } from 'react';
const axios = require('axios');

/**
 * This component will render data about a caregiver, which is sent from the "caregivers"-component
 * It will also update a caregiver's availability state.
 */
export default class Caregiver extends Component {
	constructor(props) {
		super(props);
		// Set initial state
		this.state = { caregivers: [], isChecked: this.props.available };
		this.toggleChange = this.toggleChange.bind(this);
	}

	toggleChange = async () => {
		// If the checkbutton is pressed, change the component's state...
		this.setState({
			isChecked: !this.state.isChecked,
		});
		let URI =
			'http://localhost:3002/user/update/employeeID=' +
			this.props.employeeID +
			'&available=' +
			!this.state.isChecked;
		// ...and update the caregiver's availability state in the database.
		await axios.put(URI).catch((err) => console.log(err));
	};

	render() {
		return (
			<div>
				<div className='caregiver' id={this.props.employeeID}>
					<div className='firstname'>{this.props.firstname}</div>
					<div className='surname'>{this.props.lastname}</div>
					<div className='employeeID'>{this.props.employeeID}</div>
					<input
						type='checkbox'
						className='available'
						checked={this.state.isChecked}
						onChange={this.toggleChange}
					/>
				</div>
			</div>
		);
	}
}
