import React from 'react';
import './Header.css';
import { Link } from 'react-router-dom';
import Clock from '../Clock/Clock.jsx';

class Header extends React.Component {
	logout() {
		sessionStorage.setItem('isLoggedIn', null);
		window.location.reload();
		sessionStorage.setItem('user', null);
	}
	render() {
		return (
			<div id='header'>
				<Clock id='clock' />
				<ul id='menu'>
					<li>
						<Link to='/'>Dashboard</Link>
					</li>
					<li>
						<Link to='/search'>Search and register</Link>
					</li>
					<li>
						<Link to='/register'>Register</Link>
					</li>
					<li>
						<Link to='/caregivers'>Caregivers</Link>
					</li>
					<li>
						<Link to='/profile'>
							{' '}
							{sessionStorage.getItem('user')}
						</Link>
					</li>
					<li onClick={this.logout}>
						<Link to='/'>Log out</Link>
					</li>
				</ul>
				<hr />
			</div>
		);
	}
}

export default Header;
