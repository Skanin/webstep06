const axios = require('axios');
const localhost = 'http://localhost:3002/patient/';

/**
 * Gets patient object based on pnr
 *
 * @param {string} pnr - pnr of patient to be returned
 *
 * @return {Object} patient - patient object
 */
export async function getPatient(pnr) {
	let patient = [];
	await fetch(localhost + 'pnr=' + pnr)
		.then((result) => {
			return result.json();
		})
		.then((p) => {
			patient = p[0];
		});
	return patient;
}

/**
 * Get patient's room based on pnr
 *
 * @param {string} pnr
 *
 * @return {Object} room - Room objecr or empty object.
 */
export async function getRoom(pnr) {
	let room = {};
	await fetch(localhost + 'room/pnr=' + pnr).then((result) => {
		room = result.json();
	});
	return room;
}

/**
 * Check in or out an patient.
 *
 * @param {string} pnr
 * @param {boolean} checkin - true if checking in, false if checking out
 * @param {Object} room - room object to which the patient will be checked in to. Do not use this param if cheking out
 * @param {Object} watch - wristband object to which the patient will be assigned. Do not use this param if checking out
 *
 * @return {boolean} flag - returns true or false given the success of the check in/out.
 */
export async function checkInOutPatient(
	pnr,
	checkin,
	room = null,
	watch = null
) {
	let flag = false;
	if (!checkin) {
		await axios.put(localhost + 'checkout/pnr=' + pnr).then(() => {
			flag = true;
		});
	} else {
		await axios
			.put(localhost + 'checkin/pnr=' + pnr + '&room=' + room.parent_id)
			.then(() => {
				flag = true;
			});
	}
	return flag;
}

/**
 * Gets patient based on name
 *
 * @param {string} name - name of the patient
 *
 * @return {array} patients - patient object
 */
export async function getPatientFromName(name) {
	let patients = [];
	await fetch(localhost + 'name=' + name)
		.then((result) => {
			return result.json();
		})
		.then((res) => {
			patients = res;
		});
	return patients;
}

export async function getCheckedInStatus(pnr) {
	let status;
	await fetch(localhost + '/check_in/pnr=' + pnr)
		.then((result) => {
			return result.json();
		})
		.then((res) => {
			status = res;
		});
	return status;
}
