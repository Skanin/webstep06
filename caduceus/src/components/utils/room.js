// const fetch = require('node-fetch');
const localhost = "http://localhost:3002/room/";
/**
 * Gets all rooms in the database
 *
 * return: [{_id: ObjectId, parent_id: String, name: String, owner: Int, available: boolean}, {_id: ObjectId, parent_id: String, name: String, owner: Int, available: boolean}]
 */
export async function getAll() {
    let rooms = [];
    await fetch(localhost + "/all").then((result) => {
        result = result.json();
    });
    return rooms;
}

/**
 * Gets all available rooms in the database
 *
 * return: [{_id: ObjectId, parent_id: String, name: String, owner: Int, available: boolean}, {_id: ObjectId, parent_id: String, name: String, owner: Int, available: boolean}]
 */
export async function getAvailableRooms() {
    let rooms = [];
    await fetch(localhost + "/available").then((result) => {
        rooms = result.json();
    });
    return rooms;
}

/**
 * Gets owner of a room
 *
 * @param {string} parent - parent_id of room
 *
 * @return {object} owner - patient object
 */
export async function getOwnerFromParentId(parent) {
    let owner = {};
    await fetch(localhost + "/owner/parent_id=" + parent).then((result) => {
        owner = result.json()[0];
    });
    return owner;
}

/**
 * Gets room name based on parent_Id.
 *
 * @param {string} parent
 *
 * @return {Object} room - room object or {}
 */
export async function getRoomNameFromParentId(parent) {
    let room = {};
    await fetch(localhost + "/name/parent_id=" + parent).then((result) => {
        room = result.json()[0];
    });
    return room;
}

/**
 * Gets room based on Object_Id
 *
 *  @param {int} _id - Object_Id of room
 *
 *  @return {Object} room - requested room or {}.
 */
export async function getRoomFromObjectId(_id) {
	let room = {};
	await fetch(localhost + '/id/' + _id).then((result) => {
		room = result.json()[0];
	});
	return room;
}

/**
 * Get random available room
 *
 * @return {Objecr} - random available room object.
 */
export async function getRandomRoom() {
    let rooms = await getAvailableRooms();
    console.log(rooms[Math.floor(Math.random() * rooms.length)]);
    return rooms[Math.floor(Math.random() * rooms.length)];
}
