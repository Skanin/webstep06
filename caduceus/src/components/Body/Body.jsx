import React from 'react';
import './Body.css';
import Alarm from '../Alarm/Alarm';
const fetch = require('node-fetch');

/**
 * This component is a "placeholder" for alarms. It renderers all alarm components with data fetched from the MQTT module
 */
class Body extends React.Component {
	constructor() {
		super();
		this.state = {
			// Set the initial alarms to be no alarms.
			alarms: [],
		};
	}

	async componentDidMount() {
		// Make the component fetch alarms every .5 seconds.
		this.intervalID = setInterval(() => this.update(), 500);
	}

	update() {
		// Fetch alarms from MQTT
		fetch('http://localhost:8181/')
			.then((alarms) => alarms.json())
			.then((a) => {
				this.setState({
					// Set state with alarms, even if they are not running.
					alarms: a,
				});
			})
			.catch((err) => {
				// Log any errors that may occur
				console.log(err);
			});
	}

	render() {
		return (
			<div id='container'>
				<div id='alarmHeaders'>
					<div id='residentImgHeader'>Bilde</div>
					<div id='roomHeader'>Rom</div>
					<div id='nameHeader'>Navn</div>
					<div id='timeHeader'>Tid</div>
					<div id='alarmHeader'>Alarm</div>
				</div>
				<div id='alarms'>
					{this.state.alarms
						// Filter the alarms so that only running alarms will show on the dashboard.
						.filter((alarm) => alarm.status)
						.sort((a, b) => {
							// Sort them based on the time the alarm was fired.
							var keyA = new Date(a.time),
								keyB = new Date(b.time);
							// Compare the 2 dates
							if (keyA > keyB) return -1;
							if (keyA < keyB) return 1;
							return 0;
						})
						// Map the alarms to alarm components.
						.map((a) => (
							<Alarm
								key={a.id}
								id={a.id}
								time={a.time}
								parent={a.last_known_parent}
							/>
						))}
				</div>
			</div>
		);
	}
}

export default Body;
