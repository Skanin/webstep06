import React, { Component } from 'react';
/*
Hentet klokkeinspo herifra
https://openclassrooms.com/en/courses/4286486-build-web-apps-with-reactjs/4286711-build-a-ticking-clock-component
*/

/**
 * This component will show a clock in the header.
 */
class Clock extends Component {
	constructor(props) {
		super(props);
		this.state = {
			time: new Date().toLocaleString().substring(10),
		};
	}
	componentDidMount() {
		// Make the component update every .1 second.
		this.intervalID = setInterval(() => this.tick(), 100);
	}
	// Tick the clock (update the seconds in the state).
	tick() {
		this.setState({
			time: new Date().toLocaleString().substring(10),
		});
	}
	render() {
		return <div id={this.props.id}>{this.state.time}</div>;
	}
}

export default Clock;
