import React, { useState, useEffect } from 'react';
import './Login.css';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const getPassword = require('./getPassword');

toast.configure(); //Neat messages at the top right corner.

/**
 * This is a component to handle login to the page.
 * It wil ask for username and password and compare them to what is stored in the database.
 *  */
export const Login = ({ setIsLoggedIn }) => {
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	var bcrypt = require('bcryptjs');
	const handleLogin = async () => {
		//encrypt the password.
		let pass = await getPassword.getPassword(username);
		bcrypt.compare(password, pass, function (err, res) {
			//Bcrypt comares the password in plaintext with the encrypted password.
			//This is efficient since the password is encrypted with a uniqe salt backend.
			if (res === true) {
				sessionStorage.setItem('user', username);
				sessionStorage.setItem('isLoggedIn', 'true');
				setIsLoggedIn(true);
				toast.success('Welcome ' + username);
			} else {
				toast.error('Wrong password, try again.');
			}
		});
	};
	//Updates the password an username value.
	const handleOnchange = (event) => {
		if (event.target.id === 'username') {
			setUsername(event.target.value);
		} else {
			setPassword(event.target.value);
		}
	};
	useEffect(() => {
		//If the user is already authenticated during the same session, it wont be
		//neccesary to login again And the user gets redirected to /.
		if (sessionStorage.getItem('isLoggedIn') === 'true') {
			setIsLoggedIn(true);
		}
	});

	let handleEnter = (e) => {
		if (e.key === 'Enter') {
			console.log('do validate');
			document.getElementById('loginBTN').click();
		}
	};

	return (
		<div
			className='Login'
			id='LoginMenu'
			onKeyPress={(e) => {
				handleEnter(e);
			}}>
			<label form='login'>Username</label>
			<input
				id='username'
				type='text'
				onChange={(e) => {
					handleOnchange(e);
				}}
			/>{' '}
			<br></br>
			<label htmlFor='password'>Password</label>
			<input
				id='password'
				type='password'
				onChange={(e) => {
					handleOnchange(e);
				}}
			/>
			<br></br>
			<input
				id='loginBTN'
				onClick={handleLogin}
				type='button'
				value='Log in'
			/>
		</div>
	);
};
