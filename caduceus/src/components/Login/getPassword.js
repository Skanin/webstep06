export function getPassword(username) {
    return fetch('http://localhost:3002/user/login/user=' + username)
        .then((results) => results.json())
        .then((data) => {
            //Retrive a json object on this format: [username, password, salt]
            //return bcrypt.hash(data.password, natriumclorid);
            return data.password;
        })
        .catch((err) => {
            console.log(err);
        });
}