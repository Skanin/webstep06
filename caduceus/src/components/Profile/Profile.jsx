import React, { Component } from 'react';
import './Profile.css';
import gear from './gear.gif';
import { toast } from 'react-toastify';
import TextyAnim from 'rc-texty';

/**
 * This component renderers a careviger's profile.
 */
class Profile extends Component {
	constructor() {
		super();
		this.state = {
			isLoading: true,
			available: null,
			user: null,
		};
	}

	// Handle "refresh"-button click.
	handleRefreshStatus = () => {
		// Fetch availability status from the database.
		fetch(
			'http://localhost:3002/user/available/employeeID=' +
				this.state.employeeID
		)
			.then((response) => response.json())
			.then((available) => {
				// And set the component's state with the careviger's status
				this.setState({ available: available });
				toast.success('Updated');
			})
			.catch((err) => toast.error('Something went wrong!\n' + err));
	};

	// Before the component mounts ...
	UNSAFE_componentWillMount = () => {
		//...fetch the logged in user's information from the database
		if (this.state.isLoading) {
			fetch(
				'http://localhost:3002/user/login/user=' +
					sessionStorage.getItem('user')
			)
				.then((user) => user.json())
				// and set the component's state with teir info.
				.then((user) => this.setState(user))
				.then(this.setState({ isLoading: false }));
		}
	};

	render() {
		if (this.state.isLoading) {
			return (
				<div id='boxLoading'>
					<img src={gear} alt='loading'></img>
				</div>
			);
		} else {
			return (
				<div id='box'>
					<h2>{this.state.fullname}</h2>
					<h1>
						<TextyAnim
							type='flash'
							onEnd={(type) => {
								console.log(type);
							}}>
							{!this.state.available &&
							this.state.available !== null
								? 'Unavailable'
								: 'Available'}
						</TextyAnim>
					</h1>
					<img src={this.state.IMG} alt='profileImage'></img>
					<br></br>
					<button id='BTN' onClick={this.handleRefreshStatus}>
						Refresh
					</button>
					<h3>Username: {this.state.username}</h3>
					<h3>Employee ID: {this.state.employeeID}</h3>
					<h3>Firstname: {this.state.firstname}</h3>
					<h3>Middlename: {this.state.middlename}</h3>
					<h3>Lastname: {this.state.lastname}</h3>
				</div>
			);
		}
	}
}

export default Profile;
