import React, { Component } from 'react';
import Register from '../Register/Register';
import './Checkin.css';
const { getPatient, checkInOutPatient } = require('../utils/patient');
const { getRandomRoom } = require('../utils/room');

/**
 * This component is responsible for the check in and check out of patients.
 * If a patient is not registered, the component also has an option where it can render a register component.
 */
export default class Checkin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isRegisterAvailable: false,
			ssn: '',
			hasCheckedIn: false,
			message: '',
		};
	}

	// Handle the input field. Update the state with what's inputed.
	handleChange(e) {
		this.setState({
			ssn: e.target.value,
		});
	}

	async handleClick(event) {
		// Prevent the page from reloading
		event.preventDefault();

		// Get the enetered patient
		let patient = await getPatient(this.state.ssn);

		// Check in or out the patient. If the patient is checked in - check them out, else: check them in.
		patient.checked_in
			? (await checkInOutPatient(this.state.ssn, false))
				? this.setState({ message: 'Sjekket ut ' + patient.fullname })
				: this.setState({ message: 'Noe gikk galt' })
			: (await checkInOutPatient(
					this.state.ssn,
					true,
					await getRandomRoom()
			  ))
			? this.setState({ message: 'Sjekket inn ' + patient.fullname })
			: this.setState({ message: 'Noe gikk galt' });
	}

	render() {
		return (
			<div>
				<div id='SSNinput'>
					<input
						id='SSNsearch'
						type='text'
						onChange={(e) => this.handleChange(e)}
					/>
					<input
						onClick={(e) => this.handleClick(e)}
						type='button'
						value='Sjekk inn/Sjekk ut'
					/>
				</div>
				<div
					id='isRegistered'
					onClick={() => {
						this.setState({
							isRegisterAvailable: !this.state
								.isRegisterAvailable,
						});
					}}>
					Ny pasient? Klikk her!
				</div>
				{this.state.isRegisterAvailable ? <Register /> : null}
				{this.state.message !== '' ? <p>{this.state.message}</p> : null}
			</div>
		);
	}
}
