import React, { Component } from 'react';
import './Caregivers.css';
import Caregiver from '../Caregiver/Caregiver';

/**
 * This component works a bit like body and works as a "placeholder" for all caregiver components.
 * It will fetch all data about the caregivers and render the caregiver components.
 */
export default class Caregivers extends Component {
	constructor(props) {
		super(props);
		this.state = { caregivers: [], isChecked: true };
	}
	async componentDidMount() {
		// When the component mounts, fetch all caregivers from the database.
		await fetch('http://localhost:3002/user/all')
			.then((result) => {
				return result.json();
			})
			.then((data) => {
				// Then set this component's state with all caregivers...
				this.setState({
					caregivers: data
						// ...except for the admin user.
						.filter((user) => user.employeeID !== 6969)
						.map((c) => (
							// Then map all the caregivers to caregiver components.
							<Caregiver
								key={c.employeeID}
								employeeID={c.employeeID}
								firstname={c.firstname}
								lastname={c.lastname}
								available={c.available}
							/>
						)),
				});
			});
	}

	render() {
		return (
			<div>
				<div className='rowHeader'>
					<div className='firstname'>Fornavn</div>
					<div className='surname'>Etternavn</div>
					<div className='employeeID'>Ansattnummer</div>
					<div className='available'>Tilgjengelig</div>
				</div>
				{/* Render all caregivers */}
				<div>{this.state.caregivers}</div>
			</div>
		);
	}
}
