import React, { Component } from 'react';
import './Register.css';
import axios from 'axios';
const fetch = require('node-fetch');

/**
 * This component will register a new patient in the database.
 */
export default class Register extends Component {
	constructor() {
		super();
		this.state = {
			data: {
				device_id: '',
				firstname: '',
				middlename: '',
				lastname: '',
				checked_in: false,
				pnr: '',
				medicalHistory: '',
				fullname: '',
			},
			message: '',
			watchesSelect: (
				<option disabled>Ingen klokker tilgjengelige</option>
			),
			roomsSelect: <option disabled>Ingen rom tilgjengelige</option>,
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.generateDropdowns = this.generateDropdowns.bind(this);
	}

	componentDidMount() {
		this.generateDropdowns();
	}
	// Handle a change in the form
	async handleChange(event) {
		// Prevent the from from reloading the page
		event.preventDefault();

		// Save the state in a separate variable
		let data = this.state.data;
		// Switch based on which field in the form is changed. Set the state based on that.
		switch (event.target.id) {
			case 'firstname':
				data.firstname = event.target.value;
				this.setState({ data });
				break;
			case 'middlename':
				data.middlename = event.target.value;
				this.setState({ data });
				break;
			case 'lastname':
				data.lastname = event.target.value;
				this.setState({ data });
				break;
			case 'pnr':
				data.pnr = event.target.value;
				this.setState({ data });
				break;
			case 'medicalHistory':
				data.medicalHistory = event.target.value;
				this.setState({ data });
				break;
			case 'chooseRoom':
				await fetch(
					'http://localhost:3002/room/id/' + event.target.value
				)
					.then((res) => {
						return res.json();
					})
					.then((r) => {
						this.setState({
							room: r,
						});
					})
					.catch((err) => {
						console.log(err);
					});
				break;
			case 'chooseClock':
				await fetch(
					'http://localhost:3002/wristband/id/' + event.target.value
				)
					.then((res) => {
						return res.json();
					})
					.then((w) => {
						data.device_id = w.device_id;
						this.setState({
							watch: w,
							data: data,
						});
					})
					.catch((err) => {
						console.log(err);
					});
				break;
			default:
				break;
		}
	}

	// Generate the watch and room dropdowns with available rooms and watches.
	async generateDropdowns() {
		let availableWatches;
		// Fetch watches.
		await fetch('http://localhost:3002/wristband/available')
			.then((result) => {
				return result.json();
			})
			.then((d) => {
				availableWatches =
					Object.keys(d).length === 0 && d.constructor === Object
						? []
						: d;
			});

		let availableRooms;
		// Fetch rooms
		await fetch('http://localhost:3002/room/available')
			.then((result) => {
				return result.json();
			})
			.then((d) => {
				availableRooms = d;
			});

		// Set state with available rooms and watches, as jsx elements.
		this.setState({
			watchesSelect: availableWatches.map((w) => (
				<option key={w._id} value={w._id}>
					{w.name}
				</option>
			)),
			roomsSelect: availableRooms.map((room) => (
				<option key={room._id} value={room._id}>
					{room.name}
				</option>
			)),
			availableRoomAndClock:
				availableRooms.length > 0 && availableWatches.length > 0,
			availableRooms: availableRooms,
			availableWatches: availableWatches,
		});
	}

	// Handle form submit.
	// This one is ugly, but should be intuitive.
	handleSubmit(event) {
		// Prevent the form from reloading the page.
		event.preventDefault();

		// Fetch to see if there is already a patient with the inputed ssn.
		fetch('http://localhost:3002/patient/pnr=' + this.state.data.pnr)
			.then((res) => {
				return res.json();
			})
			.then((d) => {
				// If the patient already exists, let the user know.
				if (Object.keys(d).length > 0) {
					this.setState({
						message:
							'En pasient med dette fødselsnummeret er allerede registret!',
					});
				} else {
					try {
						// If not, post the inserted patient to the database.
						axios
							.post(
								'http://localhost:3002/patient/new',
								this.state.data
							)
							.then((res) => {
								// If the user also want to check in the patient
								if (this.state.data.checked_in) {
									// Update the patient with the selected watch
									axios
										.put(
											'http://localhost:3002/wristband/update/available=' +
												!this.state.watch.availabe +
												'&_id=' +
												this.state.watch._id +
												'&owner=' +
												this.state.data.pnr
										)
										.then(
											// and then update the patient with the selected room.
											axios
												.put(
													'http://localhost:3002/room/update/available=' +
														!this.state.room
															.avaiable +
														'&_id=' +
														this.state.room._id +
														'&owner=' +
														this.state.data.pnr
												)
												.then(
													this.setState({
														message: `${res.data.firstname} ${res.data.lastname} ble registrert og sjekket inn på rom ${this.state.room.name} med klokke ${this.state.watch.name}!`,
													})
												)
												.catch((err) => {
													this.setState({
														message: `${res.data.firstname} ${res.data.lastname} ble registrert og sjekket inn med klokke ${this.state.watch.name}, men fikk ikke tildelt rom. Tildel rom manuelt. `,
													});
													console.log(err);
												})
										)
										.catch((err) => {
											this.setState({
												message: `${res.data.firstname} ${res.data.lastname} ble ikke registrert eller sjekket inn. Noe gikk galt. Prøv igjen!`,
											});
											console.log(err);
										});
								} else {
									this.setState({
										message: `${res.data.firstname} ${res.data.lastname} ble registrert!`,
									});
								}
							});
					} catch (err) {
						console.log(err);
					}
				}
			})
			.catch((err) => {
				console.log(err);
			});
	}

	render() {
		return (
			<div id='formDiv'>
				<form onSubmit={(e) => this.handleSubmit(e)}>
					<label>
						Fornavn:{' '}
						<input
							id='firstname'
							label='Fornavn'
							type='text'
							placeholder='Fornavn'
							required
							value={this.state.firstname}
							onChange={(e) => this.handleChange(e)}></input>
					</label>
					<label>
						Mellomnavn:{' '}
						<input
							id='middlename'
							label='Mellomnavn'
							type='text'
							placeholder='Mellomnavn'
							value={this.state.middlename}
							onChange={(e) => this.handleChange(e)}></input>
					</label>
					<label>
						Etternavn:{' '}
						<input
							id='lastname'
							label='Etternavn'
							type='text'
							required
							placeholder='Etternavn'
							value={this.state.lastname}
							onChange={(e) => this.handleChange(e)}></input>
					</label>
					<label>
						Personnummer:{' '}
						<input
							id='pnr'
							type='text'
							minLength='11'
							maxLength='11'
							required
							value={this.state.pnr}
							onChange={(e) => this.handleChange(e)}></input>
					</label>
					<label htmlFor='diagnoser'>
						Diagnoser:{' '}
						<textarea
							id='medicalHistory'
							type='textarea'
							placeholder='Diagnoser'
							value={this.state.medicalHistory}
							onChange={(e) => this.handleChange(e)}></textarea>
					</label>
					<label>Vil du også sjekke inn denne personen?</label>
					<input
						type='checkbox'
						id='checkInBox'
						name='checkInBox'
						onChange={() => {
							let data = this.state.data;
							data.checked_in = !this.state.data.checked_in;
							this.setState({
								data,
							});
						}}
						checked={this.state.checked_in}></input>
					{this.state.data.checked_in ? (
						<select
							defaultValue='Velg klokke'
							id='chooseClock'
							onChange={(e) => this.handleChange(e)}
							required>
							<option disabled>Velg klokke</option>{' '}
							{this.state.watchesSelect}{' '}
						</select>
					) : null}
					{this.state.data.checked_in ? (
						<select
							defaultValue='Velg rom'
							id='chooseRoom'
							onChange={(e) => this.handleChange(e)}
							required>
							<option disabled value='Velg rom'>
								Velg rom
							</option>{' '}
							{this.state.roomsSelect}{' '}
						</select>
					) : null}
					<br />
					{this.state.availableRoomAndClock ? (
						<button type='submit'>Registrer pasient</button>
					) : (
						<button type='submit' disabled>
							Registrer pasient
						</button>
					)}
				</form>
				{this.state.message !== '' ? (
					<p className='message'>{this.state.message}</p>
				) : null}
			</div>
		);
	}
}
