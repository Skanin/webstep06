import React, { Component } from 'react';
import './ExtraInfo.css';

/**
 *	This is a component to show extra info about an alarm when it is clicked.
 *	It will show Firstname and lastname and the alarmee's medical history.
 */
export class ExtraInfo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: this.props.data,
		};
	}

	render() {
		return (
			<div className='extrainfo'>
				<div>
					<h1 className='extraInfoHeader'>
						{this.state.data.firstname} {this.state.data.lastname}
					</h1>
					{this.state.data.medicalHistory}
				</div>
			</div>
		);
	}
}

export default ExtraInfo;
