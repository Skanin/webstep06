// Pad the timer with zeroes
export function pad(num) {
	return ('0' + num).slice(-2);
}

// Convert secondt to HH:MM:SS (for the timer).
export function hhmmss(secs) {
	var minutes = Math.floor(secs / 60);
	secs = (secs % 60) + 1;
	minutes = minutes % 60;
	return `${pad(minutes)}:${pad(secs)}`;
}

export function sleep(ms) {
	return new Promise((resolve) => setTimeout(resolve, ms));
}
