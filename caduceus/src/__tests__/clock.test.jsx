import React from 'react';
import Clock from '../components/Clock/Clock';
import renderer from 'react-test-renderer';

test('clock', () => {
	const clocking = renderer.create(<Clock />).toJSON();
	expect(clocking.children).toEqual([
		new Date().toLocaleString().substring(10)
	]);
});

test('foo', () => {
	expect(3 + 2).toBe(5);
});
