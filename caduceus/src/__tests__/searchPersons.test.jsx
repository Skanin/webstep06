import React from 'react';
import SearchPersons from '../components/SearchPersons/SearchPersons';
import renderer from 'react-test-renderer';

describe('SearchPersons component', () => {
	const searchPersons = renderer.create(<SearchPersons />);

	it('Renderers', () => {
		const root = searchPersons.root;
		expect(root.findAllByType('div').length).toBe(5);
		expect(root.findAllByType('input').length).toBe(2);
	});

	it('Knows that 1+2 is 3', () => {
		expect(1 + 2).toEqual(3);
	});
});
