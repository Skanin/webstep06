import React from 'react';
import Body from '../components/Body/Body';
import 'babel-polyfill';
import { mount } from 'enzyme';

describe('Testing body', () => {
	let body;

	it('renderers one alarm', async (done) => {
		body = mount(<Body />);
		body.setState({
			alarms: [
				{
					id: 'cefb923f175d',
					last_known_parent: 'feb1cd8c6dea',
					time: new Date(),
					status: true,
				},
			],
		});
		body.mount();
		expect(body.find('#alarms').children().length).toBe(1);
		done();
	});

	it('renderers two alarms', async (done) => {
		body = mount(<Body />);
		body.setState({
			alarms: [
				{
					id: 'cefb923f175d',
					last_known_parent: 'feb1cd8c6dea',
					time: new Date(),
					status: true,
				},
				{
					id: 'd31e9c4ba337',
					last_known_parent: 'feb1cd8c6dea',
					time: new Date(),
					status: true,
				},
			],
		});
		body.mount();

		expect(body.find('#alarms').children().length).toBe(2);
		done();
	});

	it('renderers alarms in order, based on time fired', async (done) => {
		body = mount(<Body />);
		let date = new Date('April 16, 2020 10:00:00');
		let date1 = new Date('April 16, 2020 09:00:00');
		body.setState({
			alarms: [
				{
					id: 'testKlokke1',
					last_known_parent: 'feb1cd8c6dea',
					time: date1,
					status: true,
				},
				{
					id: 'testKlokke2',
					last_known_parent: 'feb1cd8c6dea',
					time: date,
					status: true,
				},
			],
		});
		setTimeout(() => {
			body.update();
			expect(body.find('.alarmName').at(0).text()).toBe('Test2');
			expect(body.find('.alarmName').at(1).text()).toBe('Test');
			done();
		}, 1000);
		let body2 = mount(<Body />);
		body2.setState({
			alarms: [
				{
					id: 'testKlokke1',
					last_known_parent: 'feb1cd8c6dea',
					time: date,
					status: true,
				},
				{
					id: 'testKlokke2',
					last_known_parent: 'feb1cd8c6dea',
					time: date1,
					status: true,
				},
			],
		});
		setTimeout(() => {
			expect(body2.find('.alarmName').at(0).text()).toBe('Test');
			expect(body2.find('.alarmName').at(1).text()).toBe('Test2');
			done();
		}, 1000);
	});
});
