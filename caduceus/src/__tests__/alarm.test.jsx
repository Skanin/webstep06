import React from 'react';
import Alarm from '../components/Alarm/Alarm';
import 'babel-polyfill';
import { shallow, mount } from 'enzyme';
import { hhmmss } from '../helpers.js';

describe('Testing alarm', () => {
	let alarm;

	it('renderers with the correct patient name', async (done) => {
		alarm = shallow(
			<Alarm id='testKlokke1' parent='feb1cd8c6dea' time={new Date()} />
		);
		setTimeout(() => {
			alarm.update();
			expect(alarm.find('.alarmName').text()).toBe('Test');
			done();
		}, 300);
	});

	it('renderers with the correct room name', async (done) => {
		alarm = shallow(
			<Alarm id='cefb923f175d' parent='feb1cd8c6dea' time={new Date()} />
		);
		setTimeout(() => {
			alarm.update();
			expect(alarm.find('.alarmRoom').text()).toBe('Room A1');
			done();
		}, 300);
	});

	jest.setTimeout(20000);

	it('updates room name', async () => {
		alarm = mount(
			<Alarm id='testKlokke1' parent='feb1cd8c6dea' time={new Date()} />
		);

		await new Promise((r) => setTimeout(r, 500));

		alarm.update();
		expect(alarm.find('.alarmRoom').text()).toBe('Room A1');

		alarm.setProps({
			parent: 'fe4fdc9e9a24',
			id: 'testKlokke1',
			time: new Date(),
		});

		await new Promise((r) => setTimeout(r, 3000));
		alarm.update();
		console.log(alarm.prop('parent'));
		expect(alarm.find('.alarmRoom').text()).toBe('Room A2');
	});

	it('renderers with the correct time', () => {
		let date = new Date('April 16, 2020 10:00:00');
		alarm = shallow(
			<Alarm id='testKlokke1' parent='feb1cd8c6dea' time={date} />
		);

		setTimeout(() => {
			alarm.update();
			let time = alarm.find('.alarmTime').text().split(':')[1];
			let expectedTime = parseInt(
				hhmmss(
					Math.floor((new Date().getTime() - date.getTime()) / 1000)
				).split(':')[0]
			);
			expect(
				expectedTime - 1 <= parseInt(time) ||
					parseInt(time) <= expectedTime + 1
			).toBeTruthy();
		}, 500);
	});

	jest.setTimeout(30000);

	it('counter counting correct', async () => {
		let timeBefore;
		let timeAfter;
		let expectedTime;
		let date = new Date('April 16, 2020 10:00:00');
		alarm = shallow(
			<Alarm id='testKlokke1' parent='feb1cd8c6dea' time={date} />
		);

		setTimeout(() => {
			alarm.update();
			timeBefore = parseInt(
				alarm.find('.alarmTime').text().split(':')[1]
			);
		}, 10000);

		await new Promise((r) => setTimeout(r, 10000));

		alarm.update();

		timeAfter = parseInt(alarm.find('.alarmTime').text().split(':')[1]);
		expectedTime =
			timeBefore + 10 >= 60 ? timeBefore + 10 - 60 : timeBefore + 10;
		expect(parseInt(timeAfter) + 10 === expectedTime).toBeTruthy();
	});
});
