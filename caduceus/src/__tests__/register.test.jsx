import React from 'react';
import Register from '../components/Register/Register';
import renderer from 'react-test-renderer';
import 'babel-polyfill';
import { shallow } from 'enzyme';
const { MongoClient } = require('mongodb');

describe('Test register componnent', () => {
	let register;
	let root;

	it('rederers', () => {
		expect(root.findAllByType('div').length).toBe(1);
		expect(root.findAllByType('form').length).toBe(1);
		expect(root.findAllByType('label').length).toBe(6);
		expect(root.findAllByType('input').length).toBe(5);
		expect(root.findAllByType('textarea').length).toBe(1);
		expect(root.findAllByType('button').length).toBe(1);

		const div = root.findByType('div');

		expect(div.props.id === 'formDiv').toBeTruthy();

		const form = root.findByType('form');
	});

	let connection;
	let db;
	let userSSN = 98815561123;
	let users;

	beforeAll(async () => {
		register = renderer.create(<Register />);
		root = register.root;

		connection = await MongoClient.connect(
			'mongodb+srv://gruppe06:mongo123mongo@gruppe06-f89bj.mongodb.net/CaduceusDB',
			{
				useNewUrlParser: true,
			}
		);
		db = await connection.db('CaduceusDB');
		users = db.collection('patients');
	});

	afterAll(async () => {
		await users.deleteOne({ pnr: userSSN });
		await connection.close();
		await db.close();
	});

	jest.setTimeout(20000);

	it('Registers users', async (done) => {
		register = shallow(<Register />);
		const state = {
			data: {
				firstname: 'TestPersonFirstname',
				lastname: 'TestPersonLastName',
				middlename: 'TestPersonMiddlename',
				pnr: '98815561123',
				medicalHistory: 'Dette er en testperson',
				checked_in: false,
			},
			firstname: 'TestPersonFirstname',
			lastname: 'TestPersonLastName',
			middlename: 'TestPersonMiddlename',
			pnr: '98815561123',
			medicalHistory: 'Dette er en testperson',
			checked_in: false,
		};

		register.setState(state);

		const form = register.find('form');
		const fakeEvent = {
			preventDefault: () => console.log('preventDefault'),
		};
		form.simulate('submit', fakeEvent);
		register.update();
		const user = await users.findOne({ pnr: 98815561123 });
		expect(user !== undefined).toBeTruthy();

		setTimeout(async () => {
			register.update();
			expect(register.state().message).toEqual(
				'TestPersonFirstname TestPersonLastName ble registrert!'
			);
			done();
		}, 3000);

		setTimeout(() => {
			users.deleteOne(user);
		}, 5000);
	});
});
