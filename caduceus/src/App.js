import React, { useState } from 'react';
import Header from './components/Header/Header';
import Checkin from './components/Checkin/Checkin';
import Profile from './components/Profile/Profile';
import { Login } from './components/Login/Login';
import Body from './components/Body/Body';
import SearchPersons from './components/SearchPersons/SearchPersons';
import Caregivers from './components/Caregivers/Caregivers';
import {
	BrowserRouter as Router,
	Switch,
	Redirect,
	Route,
} from 'react-router-dom';
function App() {
	//Using react hooks to check wethre a user is logged in or not.
	const [isLoggedIn, setIsLoggedIn] = useState(false);
	return (
		<div className='App'>
			<Router>
				<Switch>
					{/* Each route has a logical statement to check wether a user is logged in or not. 
					If the user is not logged in he or she will be redirected to the login page*/}
					<Route
						path='/search'
						render={() =>
							!isLoggedIn ? (
								<Redirect to='/login' />
							) : (
								<div>
									<Header />
									<SearchPersons />
								</div>
							)
						}></Route>
					<Route
						path='/login'
						render={() =>
							isLoggedIn ? (
								<Redirect to='/' />
							) : (
								<Login setIsLoggedIn={setIsLoggedIn} />
							)
						}></Route>
					<Route
						path='/search'
						render={() =>
							!isLoggedIn ? (
								<Redirect to='/login' />
							) : (
								<div>
									<Header />
									<SearchPersons />
								</div>
							)
						}></Route>
					<Route
						path='/register'
						render={() =>
							!isLoggedIn ? (
								<Redirect to='/login' />
							) : (
								<div>
									<Header />
									<Checkin />
								</div>
							)
						}></Route>
					<Route
						path='/profile'
						render={() =>
							!isLoggedIn ? (
								<Redirect to='/login' />
							) : (
								<div>
									<Header />
									<Profile />
								</div>
							)
						}></Route>
					<Route
						path='/caregivers'
						render={() =>
							!isLoggedIn ? (
								<Redirect to='/login' />
							) : (
								<div>
									<Header />
									<Caregivers />
								</div>
							)
						}></Route>
					<Route
						exact
						path='/'
						render={() =>
							!isLoggedIn ? (
								<Redirect to='/login' />
							) : (
								<div>
									<Header />
									<Body />
								</div>
							)
						}
					/>
				</Switch>
			</Router>
		</div>
	);
}

export default App;
