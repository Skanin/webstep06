// Clear data in the session storage
clearSessionStorage = () => {
    cy.window().then((win) => {
        win.sessionStorage.clear()
    });
}
describe("React component is up and running at port=3000", ()=>{
    it("React application is up and running at localhost:3000", () => {
        clearSessionStorage();

        cy.visit("http://localhost:3000/")
        cy.get('.App').find('#LoginMenu')
        cy.get('.App').find('#username')
        cy.get('.App').find('#password')
        cy.get('.Ap02710p').find('#loginBTN')
    })
})
describe("Login security testing", () =>{
    it("Try to login without input. Get a error toast", () => {
        cy.get("#loginBTN").click()
        cy.get('.Toastify__toast').contains("Wrong password, try again.")
        cy.url("/")
    })
    it("Try to login without password. Get a error toast", () => {
        cy.get("#username").click().type("evilHacker-without-password")
        cy.get("#loginBTN").click()
        cy.get('.Toastify__toast').contains("Wrong password, try again.")
        cy.url("/")
    })
    it("Try to login without username. Get a error toast", () => {
        cy.get("#username").click().clear()
        cy.get("#password").click().type("super-secret-password")
        cy.get("#loginBTN").click()
        cy.get('.Toastify__toast').contains("Wrong password, try again.")
        cy.url("/")
    })
    it("Try to login with wrong credetials. Get a error toast", () => {
        cy.get("#password").click().clear()
        cy.get("#username").click().type("evilHacker")
        cy.get("#password").click().type("super-secret-password")
        cy.get("#loginBTN").click()
        cy.get('.Toastify__toast').contains("Wrong password, try again.")
        cy.url("/")
    })
})


describe("Login with correct credetials", () => {
    it("Login with the tartarux user", () => {
        cy.get("#username").clear().type("tartarux")
        cy.get("#password").clear().type("nurse-af")
        cy.get("#loginBTN").click()
    })
})

function testPageRoutes(innEllerUt){
    describe("Visit pages when logged "+innEllerUt, () => {
        let routeName = ["/","/search","/register","/caregivers","/profile"];    
        let nr =0;
        routeName.forEach( route => {
            it("Test page => "+route, () => {

                if (innEllerUt==="in"){
                    cy.get(':nth-child('+(nr+1)+') > a').click({force: true})
                    cy.url("http://localhost:3000"+route);
                }else {
                    
                    cy.visit("http://localhost:3000"+route)
                    cy.url("http://localhost:3000")
                }
                nr++;
    
            })}
        )
    })
}

testPageRoutes("in");


describe("Logout", () => {
    it("Logout", () => {
        cy.get(':nth-child(6) > a').click({force: true})
    })
}) 

testPageRoutes("ut");




