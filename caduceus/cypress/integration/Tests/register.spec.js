clearSessionStorage = () => {
    cy.window().then((win) => {
        win.sessionStorage.clear()
    });
}
describe("React component is up and running at port=3000", ()=>{
    it("React application is up and running at localhost:3000", () => {
        clearSessionStorage();
        cy.visit("http://localhost:3000/")
        cy.get('.App').find('#LoginMenu')
        cy.get('.App').find('#username')
        cy.get('.App').find('#password')
        cy.get('.App').find('#loginBTN')
    })
})


describe("Login", () => {
    it("Login with the tartarux user", () => {
        cy.get("#username").clear().type("tartarux")
        cy.get("#password").clear().type("nurse-af")
        cy.get("#loginBTN").click()
    })
})
let pnr = 12121210200;
let name = "Jørgen Raymond Hansen";
let illness = "Münchausen by proxy";
describe('Register a new person', () => {
    it('Go to the Register page', () => {
        cy.get(':nth-child(3) > a').click()
        cy.get('#isRegistered').click()
    });
    it('Fill in the information', () => {
        cy.get('#firstname').type("Jørgen")
        cy.get('#middlename').type("Raymond")
        cy.get('#lastname').type("Hansen")
        cy.get('#pnr').type(pnr)
        cy.get('#medicalHistory').type(illness)
        
    });
    it('Submit', () => {
        cy.get('form > button').click()
        //cy.request('DELETE', ("http://localhost:3002/patient/remove/"+pnr))

    });
    
});

describe('Controll information is correct', () => {
    it('Switch to search page and find', () => {
        cy.get(':nth-child(2) > a').click()
        cy.get('#nameSearch').type("Jørgen Raymond Hansen")
        cy.get('#searchBTN' ).click()
        cy.get(".pnr").click()
    });
    it('Controll name is correct', () => {
        cy.get('.moreInfo > :nth-child(2) > :nth-child(1) > h1').contains(name)
    });
    it('Controll PNR is correct', () => {
        cy.get(':nth-child(2) > :nth-child(1) > :nth-child(5)').contains(pnr)
    });
    it('Controll name is correct', () => {
        cy.get(':nth-child(2) > :nth-child(1) > :nth-child(7)').contains(illness)
    });
});