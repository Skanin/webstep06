module.exports = {
	verbose: true,
	collectCoverageFrom: ['src/**/*.{js,jsx,mjs}'],
	transform: {
		'^.+\\.(js|jsx|mjs)$': './jest-transformer.js',
	},
	transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs)$'],
	moduleNameMapper: {
		'\\.(scss|sass|css)$': 'identity-obj-proxy',
	},
	preset: '@shelf/jest-mongodb',
	snapshotSerializers: ['enzyme-to-json/serializer'],
	setupFiles: ['./src/setupTests.js'],
};
