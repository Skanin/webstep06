# webstep06

#### Members:

-   Thomas Alejandro Ramirez Fernandez
-   Sander Bjerklund Lindberg
-   Marius Christopher Sjøberg
-   Kaja Sofie Lundgaard
-   Ole Jacob Brunstad

## Frontend

This project is buit using a [ReactJS](https://reactjs.org/) frontend.

## Backend

This project is built using a [nodejs](https://nodejs.org/en/) backend.

# Prerequisites

## The project demands the following technologies installed:

-   [NodeJS](https://nodejs.org/en/)
-   [Git](https://git-scm.com/)

You can check whether node is installed by typing `npm -v` in a terminal window.

# Installation

## Installation - NodeJS

### MacOS

On MacOS it's preferable to have [Homebrew](https://brew.sh/index_nb) installed:

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update
export PATH="/usr/local/bin:$PATH"
```

```bash
brew install node
```

### Linux

```bash
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install nodejs
```

### Windows, MacOS and Linux

Download the latest version from [NodeJS' homepage](https://nodejs.org/en/).

## Installation - Git

### MacOS

```bash
brew install git
```

### Linux

```bash
sudo apt install git-all
```

### Windows, MacOS and Linux

Follow the installation guide on [Git's homepage](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

## Installation - Git and repository setup

### Setting up your Git user and cloning the repo

```bash
git config core.autocrlf false
git config user.name "<your github username>"
git config user.email your.github@email.com
git clone https://gitlab.com/Skanin/webstep06.git
cd webstep06
```

# Running the application

```bash
cd webstep06
npm -i
```

The application depends on three different node modules. Due to this, you will need three terminal windows open.

## Terminal window 1 - DB

```bash
cd webstep06/DB
npm -i
npm start
```

## Terminal window 2 - MQTT

```bash
cd webstep06/MQTT
npm -i
npm start
```

## Terminal window 3 - Caduceus

```bash
cd webstep06/caduceus
npm -i
npm start
```

Now that you have all terminal windows running, the application should be available on `localhost:3000`.

To turn off the applicaton, close all terminal windows either by `ctrl+c` or closing them.

# Simple use

This section will explain a few use cases.

The system do have a login-system. Credentials for the admin user are `admin:admin`.

## Registering a new patient

1.  Go to the page and click on "Sjekk inn".
2.  Click "Ikke registert? Klikk her".
3.  Fill inn all info about the patient to be registered. - "Fødselsnummer" has to be 11 digits.
4.  Choose whether you also want to check in the patient.

    4.1 Choose room and wristband.

5.  Click "Registrer patient".

The patient is now registered and can be searched:

## Searching for a patient

1.  Go to the page and click on "Søk".
2.  Search for your patient's name or social security number.
3.  Choose the patient that is showing up.
4.  Here you can either check in or out the patient or edit their info.

## Checking in or out a patient

There are two different ways to check in or out a patient:

### 1.

1.  Go to the page and click "Sjekk in".
2.  Enter the patient's social security number.

### 2.

1.  Search for the patient
2.  Click on "Sjekk in"/"Sjekk ut".

## Edit a patient's info

1.  Search for the patient
2.  Click on edit info
3.  Change the desired info and click "oppdater pasient".

The patient is now updated.

## Alarms

Unfortunately, the actual alarm system cannot be tested unless you have the wristbands at hand.
Luckily - the team created a test page where you can mock alarms. You have two choices:

### 1. By accessing the page at [http://Thomasramirez.no](http://Thomasramirez.no)

1.  Go to [http://Thomasramirez.no](http://Thomasramirez.no) and log in.
2.  Press the "Lag alarmer" button on the front page.
3.  To remove the alarms, press "Fjern alarmer".

### 2. Locally

```bash
cd webstep06
git fetch
git checkout heroku
git pull
```

Then, follow the steps in the running the application guide above. Step 2 - the MQTT can be skipped now.
When all is up and running:

1.  Press the "Lag alarmer" button on the front page.
2.  To remove the alarms, press "Fjern alarmer".

## Caregivers

### Make yourself (un)available

1.  Login to the page with (tove:admin) (case-sensitive)
2.  Go to the tab "sykepleiere"
3.  Check or uncheck the checkbox beside Tove to make yourselkf active/inactive
4.  Go to the tab "tove" and see that you are available or unavailable.

# Testing

The project do have some tests. Both Jest and Cypress. However, they are a little tricky to run. This section provides a guide in how to test.

## Jest

To make the jest tests work, the MQTT module has to be **_offline_**.
This is because the body component continuously updates it's state with alarms from MQTT and therefore overwriting the state set in the test.
Furthermore, all tests depends on the database, so the DB module has to be running when running the tests.

### How to

Make sure the DB module is running and that the MQTT is **_not_**.

```bash
cd webstep06/caduceus
npm test alarm
npm test body
npm test clock
```

It is usually enough to run `npm test` only, however this will also run the Cypress tests, but not in the right way. Therefore, it is more safe to rund the tests individually.

## Cypress

Make sure all modules are running.

```
cd webstep06/caduceus
npm run cypress
```
